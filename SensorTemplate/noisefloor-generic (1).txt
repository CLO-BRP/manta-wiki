% Noise floor data for use with Manta software (tab delimited) 
% This table provides information on the system noise floor in Power Spectral Density
% Values for underwater recorder in dB re 1 uPa^2/Hz or for in-air recorder in dB re 20uPa^2/Hz

%Recorder
Medium	underwater	%either underwater or in-air	
Recorder_type	AMAR	%example for Jasco Inc. AMAR recorder model
Recorder_ID   AMAR3451 	%example for Jasco Inc. AMAR recorder serial number
Sampling rate	200000	%Sampling rate in Hz used to measure noise floor
Gain	25	%Gain in dB used to measure noise floor

% Noise floor table
		
Freq_Hz	Noisefloor	
1	65
10	59
50	48	
100	48	
500	40	
1000	38	
5000	33
10000	32
100000	32
	
	
