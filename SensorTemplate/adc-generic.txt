% Analog-to-digital converter (ADC) data for use with Manta software (tab delimited) 
% This table provides information on the clipping level of the ADC and the anti-aliasing fliter

ADC_type	differential	%either differentail or single-ended
ADC_model	RH-ADC-VER3	%Example for a Cornell Rockhopper ADC model
ADC_serial	RH-ADC-VER3-001	%Example for a Cornell Rockhopper ADC serial number
Max_volt	5	%Maximum ADC input voltage; also known as clipping level
Min_volt	-5	%Minimum ADC input voltage; also known as clipping level

% Gain table (optional). If the freqeuncy ADC curve is not available or known, delete table below.
% Make that sure the maximum frequency equals or exceeds the Nyquist frequency of the recordings (fs/2)
		
Freq_Hz	Gain_dB	
0	0.0	
10	0.0	
100	0.0	
1000	0.0	
10000	0.0
60000	0.0	
70000	-4.0	
80000	-16.5	
90000	-71.3		
100000	-75.0	
