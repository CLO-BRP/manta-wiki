function [timeEpoch,timeStr] = mantaTime(timeDatetime)
%mantaTime      convert datetime objects or datenum values to MANTA format
%
% [timeEpoch,timeStr] = mantaTime(timeDatetime)
%   Given one or more times, return them formatted in the two ways they appear
%   in a MANTA netCDF file.
%   input:
%      timeDatetime  one or more datetime objects or datenum values
%   output:
%      timeEpoch     (double) number of seconds since 1970-01-01
%      timeStr       (string) text string in the format yyyy-mm-dd HH:MM:SS.FFFZ
%                    where 'Z' is a fixed value meaning UTC.
%
% See also datetime, datenum, datestr, writePsdNetCDF.

% The numeric value(s), number of seconds since 1970-1-1 00:00:00.
timeEpoch = seconds(timeDatetime - datetime('1970-01-01'));

% Text string(s).
timeStr = datestr(timeDatetime(:), 'yyyy-mm-dd HH:MM:SS.FFFZ');
