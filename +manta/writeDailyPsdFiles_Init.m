function h = writeDailyPsdFiles_Init(param, freqs)
%writeDailyPsdFilesInit     get ready to write daily PSD netCDF files & images
%
% ncHandle = writeDailyPsdFiles_Init(param, freqs)
%   Prepare to write daily netCDF files with power spectral density (PSD) and
%   generate daily plots of the PSDs (like spectrograms) and spectral
%   percentiles. This function merely sets things up; to actually write the
%   data, see writeDailyPsdFilesAdd. 
%
%   Use this function when you want to add PSD data incrementally -- such as
%   when there's too much data to fit in memory at once -- and use
%   writeDailyPsdFiles_Batch when you have a single array of PSDs that you want
%   to write all at once.
%
%   The usual way to use this function is to call it once to kick things off and
%   get a handle for successive calls. Then call writeDailyPsdFilesAdd as many
%   times as you like to add a succession of spectra (power spectral density
%   data), then, when done, writeDailyPsdFilesDone. writeDailyPsdFilesAdd will
%   accumulate the data that you add, writing it out to a netCDF file every time
%   there's enough data for one full day. It also removes newly-written data
%   from memory, so it's possible to process a dataset that's too large to fit
%   in memory.
%
% Inputs:
%   param               for plotting and saving files; uses these fields:
%     param.psdFileTemplate  template for making filenames; '%T' is replaced by
%                       timestamp 
%     param.<other>     see other fields used by makeDailyPsdImage,
%                       makeDailyPctileImage 
%   freqs               [1xM vector] the set of frequencies representing the
%                       center of each PSD frequency bin; in Hz
% Output:
%   ncHandle            used in follow-on calls to writeDailyPsdFilesAdd and
%                       writeDailyPsdFilesDone.
%
% See also writeDailyPsdFiles_Add, writeDailyPsdFiles_Done, 
% writeDailyPsdFiles_Batch.

% The handle just squirrels away copies of the inputs. The real work is done in
% writeDailyPsdFilesAdd.
h = struct('param', param, 'freqs', freqs, 'PSDs', zeros(0, length(freqs)), ...
  'times', [], 'locs', struct('lat', {}, 'lon', {}, 'elev', {}));
