function metricTick(varargin)
%metricTick     convert tick labels to have metric prefixes and unit suffixes
%
% metricTick(coordinate, unitname)
%   Convert the tick labels along the specified coordinate axis to have metric
%   prefixes and unitname suffixes. For instance, if your coordinates go from 0
%   to 1000000 in hertz and you do metricTick('X','Hz'), you'll get tick labels
%   using Hz, kHz, and mHz.
%
% metricTick(axes, ...)
%   If the first argument is an axes object, operate on that instead of the
%   current axes (gca), which is the default.

narginchk(2,3);

v = varargin;
if (isa(v{1}, 'matlab.graphics.axis.Axes'))
  ax = v{1};
  v(1) = [];
else
  ax = gca;
end

coord = v{1};
unit = v{2};

if (length(coord) ~= 1 || isempty(find('xyz' == lower(coord), 1)))
  error("Bad coordinate name; needs to be 'x', 'y', or 'z'.")
end

ticks = get(ax, [coord 'Tick']);
labels = cell(1, length(ticks));
for i = 1 : length(ticks)
  [newval,prefix] = manta.metricPrefix(ticks(i), 1);
  labels{i} = sprintf('%g %s%s', newval, prefix, unit);
end
set(ax, [coord 'TickLabel'], labels)
