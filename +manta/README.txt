MANTA netCDF file writer

This set of routines is for writing daily netCDF files from MANTA
spectrum data. netCDF is a self-describing file format popular in the
Earth sciences that allows you to save a series of scalar or array
variables in a file. You can optionally append to the array variables
along one of their dimensions, allowing you to accumulate data into a
netCDF file as you receive or process it.

The spectrum data you have should be one or more (usually many)
acoustic power spectra spanning a user-specified range of
frequencies. The spectral values should be in decibels re 1
uPa^2/Hz. Each spectrum should have a timestamp indicating
the start time for the acoustic data that went into the spectrum
calculation.

In addition to writing the daily netCDF files, these routines also
make daily images of the data, in two forms: spectrograms (time series
of spectral slices) and percentile plots. These daily images are for
presenting an overview of the data so you can see what's in it and
check that the data set has at least superficially reasonable data.

There are two principal ways in this code to write MANTA netCDF files,
batch and incremental. In the batch method, you have a bunch of data
spanning some long duration and you hand it over all at once. The code
here slices it up into daily chunks, and makes netCDF and image files
for each chunk. This method is relatively fast, but limited to data
sets that fit in memory.

In the incremental way, you have chunks of data that you hand off
one-by-one to this code; the code will accumulate your data, writing
out daily netCDF files and images whenever it accumulates a day's
worth of data. It then removes from memory any data it writes,
allowing you to process arbitrarily large data sets without running out
of memory. It's slower than the batch way.

The batch way is accessed via a call to

  writeDailyPsdFiles_Batch

while the incremental way is accessed like this:

  writeDailyPsdFiles_Init          % call this once at the start
  writeDailyPsdFiles_Add           % call this as many times as desired
  writeDailyPsdFiles_Done          % call this once at the end

There are examples of calls to all of these routines, using simulated
and random data, here:

    testWriteNetCDF.m

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Top-level routines:

  testWriteNetCDF.m
    script to test other routines
  
  writeDailyPsdFiles_Batch.m
    batch process spectral data into 1-day netCDF and image files

  writeDailyPsdFiles_Init.m	
  writeDailyPsdFiles_Add.m
  writeDailyPsdFiles_Done.m
    incrementally process spectral data into 1-day netCDF and image files

Intermediate-level routines:

  processDay.m
    given one day worth of spectra, make netCDF and image files

  writePsdNetCDF.m
    writes a netCDF (.nc) file with the data from a succession of spectra

  makePctileImage.m
    writes a .png image file of a percentile plot from a succession of spectra 

  makePsdImage.m
    writes a .png image file of a power spectral density plot (similar
    to a spectrogram) from a succession of spectra 

Utilities:

  iff.m
      conditional value function, similar to C ?: operator
  mantaTime.m
      converts datetime values to the time format used in MANTA netCDF files
  metricPrefix.m
      return a metric prefix like 'M', 'k', 'm', 'u', etc. for a number
  metricTick.m
      makes the tick-marks on a plot axis have metric prefixes
  percentile.m
      calculate percentiles
  samesize.m
      return whether two arrays are the same size and shape
  turbocolormap.m
      returns the 'turbo' colormap
