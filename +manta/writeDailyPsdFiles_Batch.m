function writeDailyPsdFiles_Batch(param, timesDt, freqs, sensorLocs, PSDs)
%writeDailyPsdFiles_Batch     write daily PSD netCDF files, make PSD and percentile plots
%
% writeDailyPsdFiles_Batch(param, timesDt, freqs, lats, lons, elevs, PSDs)
%   Write daily netCDF files with power spectral density (PSD) from one big
%   batch of spectrum data. Also generate daily plots of the PSDs (like
%   spectrograms) and spectral percentiles.
%
% Inputs:
%   param               for plotting and saving files; uses these fields:
%     param.psdFileTemplate  template for making filenames; any '%T' in it is 
%                       replaced by a timestamp with the date
%     param.<other>     see other fields used by makePsdImage, makePctileImage
%   timesDt             [1xN vector] a set of times at which the PSDs were
%                       calculated; each time is the *start* of the time
%                       period in which the PSD was computed; encoded as
%                       datetime (Dt) objects or datenum values
%   freqs               [1xM vector] the set of frequencies representing the
%                       center of each PSD frequency bin; in Hz
%   sensorLocs          [1x1 or 1xN struct] a single struct for a fixed
%                       instrument or, for a moving one, a vector struct with
%                       one struct per time step; has these fields:
%     sensorLocs.lat    [scalar] latitude estimate of the sensor for each PSD
%     sensorLocs.lon    [scalar] like lats but with longitudes
%     sensorLocs.elev   [scalar] elevation of the sensor above mean sea level
%                       for each PSD; in meters, generally positive for aerial
%                       and terrestrial sensors and negative for subsea ones
%   PSDs                [NxM array] spectra encoded as power spectral density
%                       values; in dB re 1 uPa^2/Hz; each row holds the spectrum
%                       for one time step, each column has the spectrum
%                       values for one frequency
%
% Outputs: None, but writes netCDF files and .png files.
%
% See also writeDailyPsdFiles_Init, writeDailyPsdFiles_Add,
% writeDailyPsdFiles_Done, writePsdNetCDF.

import manta.*

day0 = floor(datenum(min(timesDt)));
day1 =  ceil(datenum(max(timesDt)));
for t_day = day0 : day1 - 1
  % t is the start time of each hour of output data.
  t0 = datetime(t_day,                         'ConvertFrom', 'datenum');
  t1 = datetime(t_day + param.psdPlotDuration, 'ConvertFrom', 'datenum');
  
  % Get indices of the rows that are within this time span. Use 0.1 second of
  % fudging to get the rows because of floating-point exactness problems.
  ix = find((timesDt >= t0 - 0.1/(24*60*60)) & ...
            (timesDt <  t1 - 0.1/(24*60*60)));
  
  % Get lat/lon/elev data for those rows. If sensorLocs is a scalar, just use
  % it as is.
  sensorLocsForIx = sensorLocs(iff(isscalar(sensorLocs),  1, ix));
  
  % Generate the netCDF and image files.
  processDay(param, timesDt(ix), freqs, sensorLocsForIx, PSDs(ix,:));
end
