function h = writeDailyPsdFiles_Add(h, timesDt, sensorLocs, PSDs)
%writeDailyPsdFiles_Add  add power spectral data incrementally, write daily files
%
% ncHandle = writeDailyPsdFiles_Add(ncHandle, timesDt, sensorLocs, PSDs)
%   Add a succession of power spectral density (PSD) spectra to the existing set
%   of spectra. If the result completes one or more day's worth of data, write
%   out one-day file(s) in netCDF format, make 1-day spectrograms and percentile
%   plots, and remove that data from memory. The latter makes it possible to
%   process a dataset that's too large to fit in memory. The data in successive
%   calls to this function should go forward in time, i.e., timesDt should be
%   monotonically increasing both in a single call (within timesDt) and from one
%   call to the next (i.e., max(timesDt) in one call should be less than
%   min(timesDt) in the next call).
%
% Inputs:
%   ncHandle            a handle returned by writeDailyPsdFiles_Init
%   timesDt             [1xN vector] a set of times at which the PSDs were
%                       calculated; each time is the *start* of the time
%                       period in which the PSD was computed; encoded as
%                       datetime (Dt) objects or datenum values
%   sensorLocs          [1x1 or 1xN struct] a single struct for a fixed
%                       instrument or, for a moving one, a vector struct with
%                       one struct per time step; has these fields:
%     sensorLocs.lat    [scalar] latitude estimate of the sensor for each PSD
%     sensorLocs.lon    [scalar] like lats but with longitudes
%     sensorLocs.elev   [scalar] elevation of the sensor above mean sea level
%                       for each PSD; in meters, generally positive for aerial
%                       and terrestrial sensors and negative for subsea ones
%   PSDs                [NxM array] spectra encoded as power spectral density
%                       values; in dB re 1 uPa^2/Hz; each row holds the spectrum
%                       for one time step, each column has the spectrum
%                       values for one frequency
% Output
%   ncHandle            the handle, to be passed in next time
%   Also writes netCDF files and daily spectrograms and percentile plots.
%
% NB: This routine is not efficient about memory usage. It grows some internal
% buffers (h.PSDs and others) as the caller incrementally adds data and shrinks
% the buffers whenever it processes one or more days in it and then discards the
% data. If there are speed problems, this would be the first thing to address.
%
% See also writeDailyPsdFiles_Init, writeDailyPsdFiles_Done, 
% writeDailyPsdFiles_Batch, testWriteNetCDF.

if (nargout < 1)        % prevent a common error
  error('You forgot to assign the output handle in your call to this function, like\n    "ncHandle = %s(ncHandle, ...)"', ...
    mfilename);
end

% Append the new data to the end of the existing data. A scalar sensorLocs is
% converted (via replication) to a vector, which eliminates problems with things
% like input data with only 1 spectrum.
n = size(PSDs,1);               % number of new spectra being added
h.PSDs  = [h.PSDs; PSDs];       % slow!
h.times = [h.times timesDt(:).'];
if (isscalar(sensorLocs)), h.locs = [h.locs sensorLocs(ones(1,n))]; 
else, h.locs = [h.locs sensorLocs];
end

% Do we have >1 day's worth of data? If so, call processDay() on it.
% First get the day (in datenum format) of the time steps in h.times.
hDay = floor(datenum(h.times) + 0.01/(24*60*60));
ixLeft = true(1, length(hDay));   % 'true' at indices that aren't processed yet
changed = false;
while (any(ixLeft))
  % Process the data for the first day. First get indices of spectra to process,
  % which are just the indices whose day equals the first unprocessed day.
  ix =   (hDay >= hDay(find(ixLeft,1))) ...
    &    (hDay <  hDay(find(ixLeft,1)) + h.param.psdPlotDuration);
  if (~any(ixLeft)), break; end
  % Test whether we have all the data for this day. This is true when we have
  % some data for the next day -- i.e., this day is different from hDay(end).
  if (~any(hDay(ix) < hDay(end))), break; end
  manta.processDay(h.param, h.times(ix), h.freqs, h.locs(ix), h.PSDs(ix,:));
  % Remove the processed indices from the set left to do.
  ixLeft(ix) = false;
  changed = true;
end
if (changed)
  % Remove the processed days from the data to save memory.
  h.times = h.times(ixLeft);
  h.locs  = h.locs (ixLeft);
  h.PSDs  = h.PSDs (ixLeft,:);      % slow!
end
