function makePctileImage(param, namepart, freqs, PSDs)
%makePctileImage   graph spectral percentiles and save as a .png file 
%
% makePctileImage(param, namepart, freqs, PSDs)
%    Given a PSD array, make a percentile plot of the spectra and save it as a
%    .png file.
%
% Inputs:
%   param                   [struct] info for plotting, saving files; uses these fields:
%     param.pctiles         [vector] percentile values to plot, e.g. [10 25 50 75 90]
%     param.pctileImageDir  [string] where to save the .png files; if empty,
%                           these are not saved; directory is created if needed
%     param.pctilePlotSize  [2-vector] size of the plot, in pixels
%     param.pctilePlotDbLimits  [2-vector] lower and upper Y-axis limits, in dB
%   namepart                [string] name of corresponding netCDF file; used for
%                           plot title, .png filename 
%   freqs                   [1xM] frequency bin centers of PSD values, Hz
%   PSDs                    [NxM] power spectral densities, dB re 1 uPa^2/Hz
%
% Outputs: none, but writes a .png file

fname = fullfile(param.pctileImageDir, [namepart '_pctile.png']);
dirpart = fileparts(fname);

% Generate the percentile plot.
clf
p = manta.percentile(PSDs, param.pctiles/100);
lns = plot(repmat(freqs.', 1, size(p,1)), p.');   % plot many lines at once

% Fix the plot's scaling, tick labels, title.
set(gca, 'XLim', param.pctilePlotFreqLimits, 'XScale', 'log')
manta.metricTick('x', 'Hz')
set(gca, 'YLim', param.pctilePlotLevelLimits)
ylabel('PSD, dB re 1 �Pa^2/Hz')
title(namepart, 'Interpreter', 'none');
grid on

% Set size of the figure and position of the plot within it.
set(gcf, 'Units', 'pixel');
posF = get(gcf, 'Position');
set(gcf, 'Position', [posF(1:2) param.pctilePlotSize]);
set(gca, 'Units', 'pixels', 'Position', [65 30 param.pctilePlotSize-[85 60]])

% Make an inset with a key for the percentiles. This is done using a new axes
% object. There are a lot of numeric constants here that make things appear in
% the right places; just mess with them to move stuff around.
hold on
set(gca, 'Units', 'pix');
posA = get(gca, 'Position');
linespace = 17;               % spacing between lines, in pixels
x = (length(param.pctiles) + 1) * linespace;
iPos = [-85 -x-8 80 x+3];   % position (relative to upper right) & size of inset 
iax = axes(gcf, 'Units', 'pixel', 'Position', [posA(1:2)+posA(3:4)+iPos(1:2) iPos(3:4)], ...
  'Box', 'on', 'XTick', [], 'YTick', [], 'XLim', [0 iPos(3)], 'YLim', [0 iPos(4)]);
ypos = 10;                  % position of first inset line above bottom of box
for i = 1 : length(lns)     % count up so lines are stacked in same order
  line([10 35], [ypos ypos], 'Color', get(lns(i), 'Color'), 'LineWidth', 2);
  text(iax, 40, ypos, sprintf('%dth', param.pctiles(i)));
  ypos = ypos + linespace;
end
text(5, ypos, 'Percentiles:');
hold off

if (~isempty(param.pctileImageDir))
  % Save it as a .png file. First make sure the directory exists.
  if (~exist(dirpart, 'dir')), mkdir(dirpart); end
  print('-dpng', fname)
end
