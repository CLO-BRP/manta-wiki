function makePsdImage(param, namepart, timesDt, freqs, PSDs)
%makePsdImage       make an image of the PSD spectrogram and save as a .png file
%
%makePsdImage(param, namepart, timesDt, freqs, PSDs)
%   Given a chunk of the PSD array representing one day of data (and the
%   corresponding timesDt vector), make an image and save it as a .png file.
%
% Inputs:
%   param                   [struct] info for plotting, saving files; uses these fields:
%     param.psdLevelLimits  [2-vector] min/max levels to plot (&colorbar limits)
%     param.psdImageDir     [string] where to save the .png files; if empty,
%                           these are not saved; directory is created if needed
%     param.psdPlotSize     [2-vector] size of the plot, in pixels
%     param.psdPlotDuration [scalar] duration of the plot, in days
%   namepart                [string] name of corresponding netCDF file; used for
%                           plot title and names of .png files
%   timesDt                 [1xN] start time of each 1-min PSD, datetime format
%   freqs                   [1xM] frequency bin centers of PSD values, Hz
%   PSDs                    [NxM] power spectral densities, dB re 1 uPa^2/Hz
%
% Note: I've tested only 1-day files (param.psdPlotDuration=1). I'm not sure if
% the tick marks and labels will be right for other durations.
%
% Outputs: none, but writes a .png file

fname = fullfile(param.psdImageDir, [namepart '_PSD.png']);
dirpart = fileparts(fname);

% Generate the image.
clf
imagesc(datenum(timesDt), freqs, PSDs.');
colormap('manta.turbocolormap')         % colormap names must be lowercase

% Fix the plot's scaling, tick labels, title.
set(gca, 'XLim', floor(datenum(min(timesDt))) + [0 param.psdPlotDuration])
set(gca, 'YLim', param.psdPlotFreqLimits, 'YScale', 'log', 'YDir', 'normal')
caxis(gca, param.psdPlotLevelLimits);
datetick x keeplimits
manta.metricTick('y', 'Hz')
title(namepart, 'Interpreter', 'none')

% Set the size of the figure and position of the plot within it. The various
% numbers below determine how big the margins are.
set(gcf, 'Units', 'pixel');
pos = get(gcf, 'Position');
ssz = get(0, 'ScreenSize');     % make sure top of figure is on screen
set(gcf, 'Position', [pos(1) min(pos(2),ssz(4)-param.psdPlotSize(2)-100)  param.psdPlotSize]);
set(gca, 'Units', 'pixels', 'Position', [65 40 param.psdPlotSize-[180 80]])

% Add a colorbar. This auto-scales to the caxis values above.
cax = colorbar;
ylabel(cax, 'PSD, dB re 1 �Pa^2/Hz')

if (~isempty(param.psdImageDir))
  % Save it as a .png file. First make sure the directory exists.
  if (~exist(dirpart, 'dir')), mkdir(dirpart); end
  print('-dpng', fname)
end
