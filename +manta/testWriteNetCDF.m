% Create simulated data and write it to MANTA netCDF file(s). This is for
% testing writePsdNetCDF and other functions.
%
% The simulation pretends we're getting data from a glider that moves straight
% from one geographic point to another and does one dive every 6 hours. The
% spectrum data is random, generated from a mean and standard deviation.

%% %%%%%%%%%%%%%%%%%%%%% Configuration for simulation %%%%%%%%%%%%%%%%%%%%%%%
% These values are used only for testing the code. They set up the simulation of
% a glider that recorded spectra (simulated random values) while diving and
% traveling from one location to another over a given time period.
homeDir     = 'C:\Dave\matlab_repo\manta\';    % where to put test file(s)
param = struct(...          % info for output file names
  'psdDir',                 fullfile(homeDir, 'nc/'), ...
  'psdFileTemplate',        'simulation_%T.nc', ...  % '%T' is replaced by date string
  'psdImageDir',            fullfile(homeDir, 'psd/'), ...
  'pctileImageDir',         fullfile(homeDir, 'pctile/'));
% Note that other fields of 'param' are set in plot configuration below.

% 'sim' controls the simulated glider data, both the simulated spectrum and the
% simulated glider track. These numbers are completely made up.
sim = struct(...
  'minFreq',    1, ...      % lower end of frequency range
  'sRate',      96000, ...  % sample rate of sound data
  'psdMean',    100, ...    % PSD mean,  dB re 1 uPa^2/Hz
  'psdStDev',   10, ...     %  "  stdev, dB re 1 uPa^2/Hz
  'psdTilt',    -50, ...    % spectrum change from low freq to high, also dB
  'tStart',     '2020-02-01 17:23:00',...% start time of simulated glider track
  'tEnd',       '2020-02-25 14:32:00',...% end    "   "      "       "      "
  'lonLatStart',[-122 44], ...           % start loc of simulated glider track
  'lonLatEnd',  [-125 44.5],...          % end    "  "      "       "      "
  'diveHrs',    6);                      % length of each glider dive in hours
%%%%%%%%%%%%%%%%%%%%%%% End of configuration %%%%%%%%%%%%%%%%%%%%%%

%% %%%%%%%%%%%%%%%%%%%%% Configuration for plots %%%%%%%%%%%%%%%%%%%%%%%
% These determine what gets plotted, and perhaps should be part of a MANTA
% standard. First the PSD plot...
param.psdPlotLevelLimits = [50 130];    % min/max levels to show in PSD 'gram
param.psdPlotFreqLimits = [sim.minFreq sim.sRate/2];  % in Hz; maybe we should standardize?
param.psdPlotSize = [1600 500];         % in pixels; roughly 1 pixel = 1 minute
param.psdPlotDuration = 1;              % in whole days; have tested only 1 day
% ...then the percentile plot:
param.pctiles = [10 25 50 75 90];       % percentiles to show in percentile plot
param.pctilePlotLevelLimits = [30 130]; % min/max levels to show in pctile plot
param.pctilePlotFreqLimits = [sim.minFreq sim.sRate/2];  % in Hz; maybe we should standardize?
param.pctilePlotSize = [800 400];       % in pixels
%%%%%%%%%%%%%%%%%%%%%%% End of configuration %%%%%%%%%%%%%%%%%%%%%%%

%% Generate simulated data: freq bin centers, time bins, PSDs, lat/lon, etc.

% Reset random number generator so we always get the same thing.
rng('default');

% Frequency bins: Use freqs in millidecades from minFreq to Nyquist, except that
% low-frequency bins are exactly 1 Hz -- the resolution of the FFT. The
% switchover from 1-Hz-wide bins to millidecades happens at 455 Hz, where bins
% up to and including 455 are whole numbers (i.e., there's a bin center at
% 455.000000 Hz) and ones after that are millidecades. The switchover point was
% chosen because a millidecade value there, 454.9881 Hz, is quite close to 455
% Hz, so the switchover is smooth. Every bin is at least 1 Hz wide.
freqsMillidecade = ...
  10.^((round(log10(sim.minFreq) * 1000) : floor(log10(sim.sRate/2) * 1000)) / 1000);
lastLinHz = 455;
freqs = [(floor(sim.minFreq) : lastLinHz) ...   % use 1-Hz increments...
  freqsMillidecade(freqsMillidecade > lastLinHz+0.5)]; % ...then millidecades

% Time bins. The bins are spaced at intervals of 1 minute (= 1 day / 24 / 60).
nTimeStep = minutes(datetime(sim.tEnd) - datetime(sim.tStart));
timesDT = (0 : nTimeStep-1)/24/60 + datetime(sim.tStart);  % datetime objects

% Spectrum (PSD) data. Units: dB re 1 uPa^2/Hz. First make flat spectrum, then
% apply tilt.
PSDs = randn(nTimeStep, length(freqs)) * sim.psdStDev + sim.psdMean;
PSDs = PSDs + repmat(linspace(0, sim.psdTilt, length(freqs)), nTimeStep, 1);

% Lat/lon. Linearly interpolate between start and stop locations.
lons = linspace(sim.lonLatStart(1), sim.lonLatEnd(1), nTimeStep);
lats = linspace(sim.lonLatStart(2), sim.lonLatEnd(2), nTimeStep);

% Elevation. Simulate some dives. Half of each dive goes down and half comes up.
% Create depths for one dive, replicate it for as many dives as needed, trim the
% length to the number of time steps we have, and add a point at the surface (0
% elevation) at the end. This could make a huge jump at the last time step.
el = [linspace(0, -990, sim.diveHrs/2*60) linspace(-990, 0, sim.diveHrs/2*60)];
elevs = repmat(el, 1, ceil(nTimeStep / length(el)));               % many dives
elevs = [elevs(1 : nTimeStep-1)  0];   % glider jumps to elev 0 at final time

sensorLocs = struct(...
  'lat',    num2cell(lats), ...
  'lon',    num2cell(lons), ...
  'elev',   num2cell(elevs));

%% Create the output files from the simulated data.
ftemplate = fullfile(param.psdDir, param.psdFileTemplate);
tic
if (1)      % switch this between 0/1 to test batch and incremental data supply
  % Test supplying data in one batch.
  manta.writeDailyPsdFiles_Batch(param, timesDT, freqs, sensorLocs, PSDs);
else
  % Test supplying data incrementally in a series of calls. Here it's done by
  % sending one single spectrum at a time (with PSD(i,:)) in each call to
  % writeDailyPsdFiles_Add, but you could send any number of spectra in each
  % call.
  hnd = manta.writeDailyPsdFiles_Init(param, freqs);
  for i = 1 : size(PSDs,1)
    hnd = manta.writeDailyPsdFiles_Add(hnd, timesDT(i), sensorLocs(i), PSDs(i,:));
  end
  hnd = manta.writeDailyPsdFiles_Done(hnd);
end
toc

%% Summarize the contents of one of the files.
%ncdisp('nc/test_20200210T000000.nc')
