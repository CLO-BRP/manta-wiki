function h = writeDailyPsdFiles_Done(h)
%writeDailyPsdFiles_Done    finish writing daily PSD files and plots
%
% ncHandle = writeDailyPsdFiles_Done(ncHandle)
%   Clean up at the end of writing daily PSD files and images of spectrograms
%   and percentile plots. This involve writing those files and images for any
%   partial-day data left over.
%
% See writeDailyPsdFiles_Init, writeDailyPsdFiles_Add, writeDailyPsdFiles_Batch.

if (~isempty(h.times))
  manta.processDay(h.param, h.times, h.freqs, h.locs, h.PSDs);
  h.times = [];
  h.locs  = h.locs([]);
  h.PSDs  = zeros(0, length(h.freqs));
end
