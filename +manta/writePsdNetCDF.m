function writePsdNetCDF(filename, timesDt, freqs, sensorLocs, PSDs)
%writePsdNetCDF     write a netCDF file in the format of the MANTA project
%
% writePsdNetCDF(filename, timesDt, freqs, lats, lons, elevs, PSDs)
%   Given several power spectral density vectors (i.e., an array containing
%   spectra) and the associated metadata, write a netCDF file containing it. The
%   internal format of the file is netCDF-4.
%   
% Inputs:
%   filename            [string] name of the file to write, preferably a full
%                       path; usually has extension .nc
%   timesDt             [1xN vector] a set of times at which the PSDs were
%                       calculated; each time is the *start* of the time period
%                       in which the PSD was computed; encoded as datetime
%                       objects or datenum values
%   freqs               [1xM vector] the set of frequencies representing the
%                       center of each PSD frequency bin; in Hz
%   sensorLocs          [1x1 or 1xN struct] a single struct for a fixed
%                       instrument or, for a moving one, a vector struct with
%                       one struct per time step; has these fields:
%     sensorLocs.lat    [scalar] latitude estimate of the sensor for each PSD
%     sensorLocs.lon    [scalar] like lats but with longitudes
%     sensorLocs.elev   [scalar] elevation of the sensor above mean sea level
%                       for each PSD; in meters, generally positive for aerial
%                       and terrestrial sensors and negative for subsea ones
%   PSDs                [NxM array] spectra encoded as power spectral density
%                       values; in dB re 1 uPa^2/Hz; each row holds the spectrum
%                       for one time step, each column has the spectrum
%                       values for one frequency
%
% Output: Nothing is returned, but a file is written
%
% See also makePsdImage, processDay (which calls this), testWriteNetCDF.

import manta.*    % so we don't have to prefix calls with 'manta.'

[timesEpoch,timesStr] = mantaTime(timesDt);

%% Create the file. IF YOU COPY THIS CODE, CHANGE CLOBBER TO NOCLOBBER so that
% you don't accidentally overwrite existing files. The new file is formatted in
% netCDF-4, which appeared in 2008 and is now the most popular format.
if (~exist(fileparts(filename), 'dir')), mkdir(fileparts(filename)), end
ncId = netcdf.create(filename, ...               % see comment above!
    bitor(netcdf.getConstant('NETCDF4'), netcdf.getConstant('CLOBBER')));

%% Create the netCDF dimensions, variables, and attributes.
% Create dimensions.
%timeDimId = netcdf.defDim(ncId, 'time',      length(timesDt));
timeDimId = netcdf.defDim(ncId, 'time',      netcdf.getConstant('NC_UNLIMITED'));
freqDimId = netcdf.defDim(ncId, 'frequency', length(freqs));
timeStrLenDimId = netcdf.defDim(ncId, 'timeStrLen', size(timesStr,2));

% Create variables and their attributes: 
%    frequency (in Hz)
%    time (in Linux-epoch seconds)
%    power spectral density (in dB re 1 uPa^2/Hz)
freqVarId = netcdf.defVar(ncId, 'frequency',  'NC_FLOAT', freqDimId);
netcdf.putAtt(ncId, freqVarId,  'units' ,     'hertz');
netcdf.putAtt(ncId, freqVarId,  'long_name' , 'frequency_Hz');

timeVarId = netcdf.defVar(ncId, 'time',       'NC_DOUBLE', timeDimId);
netcdf.putAtt(ncId, timeVarId,  'units' ,     'seconds since 1970-01-01 00:00:00');
netcdf.putAtt(ncId, timeVarId,  'long_name' , 'time_UTC_sec_since_1970');

tsVarId = netcdf.defVar(ncId,   'time_string','NC_CHAR', [timeStrLenDimId timeDimId]);
netcdf.putAtt(ncId, tsVarId,    'units',      'n/a');
netcdf.putAtt(ncId, tsVarId,    'long_name' , 'time_UTC_as_string');

psdVarId = netcdf.defVar(ncId,  'PSD',        'NC_FLOAT', [freqDimId timeDimId]);
netcdf.putAtt(ncId, psdVarId,   'units',      'dB re 1 uPa^2/Hz');
netcdf.putAtt(ncId, psdVarId,   'long_name' , 'power_spectral_density');

lonVarId = netcdf.defVar(ncId,  'longitude',  'NC_FLOAT', timeDimId);
netcdf.putAtt(ncId, lonVarId,   'units',      'degrees');

latVarId = netcdf.defVar(ncId,  'latitude',   'NC_FLOAT', timeDimId);
netcdf.putAtt(ncId, latVarId,   'units',      'degrees');

elevVarId = netcdf.defVar(ncId, 'elevation',  'NC_FLOAT', timeDimId);
netcdf.putAtt(ncId, elevVarId,  'units',      'meters');
netcdf.putAtt(ncId, elevVarId,  'long_name' , 'elevation_above_sea_level');

% Done with definitions.
netcdf.endDef(ncId);

%% Write the data.
% Freqs are written only once. Other data are written for every time step.
netcdf.putVar(ncId, freqVarId, freqs);

lats  = [sensorLocs.lat];         % shorthand
lons  = [sensorLocs.lon];
elevs = [sensorLocs.elev];

if (1)
  % Write all the data at once. For some reason, the first putVar() call has to
  % have 'start' and 'count' values, but later ones don't. Maybe the later ones
  % inherit these by default.
  n = length(timesEpoch);                   % number of time steps
  netcdf.putVar(ncId, timeVarId, 0, n, timesEpoch);
  netcdf.putVar(ncId, tsVarId,   timesStr);
  netcdf.putVar(ncId, lonVarId,  iff(isscalar(lons),  repmat(lons, 1,n), lons));
  netcdf.putVar(ncId, latVarId,  iff(isscalar(lats),  repmat(lats, 1,n), lats));
  netcdf.putVar(ncId, elevVarId, iff(isscalar(elevs), repmat(elevs,1,n), elevs));
  netcdf.putVar(ncId, psdVarId,  PSDs);
else
  % Write data one time step at a time. This is about 10x slower but
  % accommodates generating data (incl. time step values) on the fly, taking
  % advantage of the 'UNLIMITED' dimension for each of these netCDF variables.
  for i = 1 : length(timesEpoch)                                %#ok<UNRCH>
    netcdf.putVar(ncId, timeVarId, i-1, 1, timesEpoch(i));
    netcdf.putVar(ncId, tsVarId,   [0 i-1], [size(timesStr,2) 1], timesStr(i,:));
    netcdf.putVar(ncId, lonVarId,  i-1, 1, lons(i));
    netcdf.putVar(ncId, latVarId,  i-1, 1, lats(i));
    netcdf.putVar(ncId, elevVarId, i-1, 1, elevs(i));
    netcdf.putVar(ncId, psdVarId,  [0 i-1], [length(freqs) 1], PSDs(i,:));
  end
end


%% Done. Close up.
netcdf.close(ncId)
