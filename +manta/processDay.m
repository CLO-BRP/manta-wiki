function processDay(param, timesDt, freqs, sensorLocs, PSDs)
%processDay         given one day of spectra, generate netCDF and image files
%
% processDay(param, timesDt, freqs, sensorLocs, PSDs)
%    Given a day's worth of spectra in PSDs, along with the time steps (timesDt)
%    of its first dimension and frequency steps (freqs) of its second dimension,
%    make 
%       (1) a netCDF (.nc) file with the spectra
%       (2) an image file (.png) with the PSDs displayed like a spectrogram
%       (3) an image file (.png) with a percentile plot for the PSDs
%
% Inputs:
%   param               [struct] for plotting & saving files; uses these fields:
%     param.psdDir      [string] directory for saving the .netCDF spectra file, 
%                       which is created if needed; if empty string, no such
%                       file is made
%     param.psdFileTemplate [string] template used to construct the filenames of
%                       the netCDF, PSD image, and percentile image files; any
%                       %T in the filename gets replaced with a timestamp
%                       representing the day (but not the time!) of the start of
%                       timesDt
%     params.<other>    other structure fields as used by makePsdImage and
%                       makePctileImage (q.v.)
%   timesDt             [1xN vector] a set of times at which the PSDs were
%                       calculated; each time is the *start* of the time period
%                       in which the PSD was computed; encoded as datetime
%                       objects or datenum values
%   freqs               [1xM vector] the set of frequencies representing the
%                       center of each PSD frequency bin; in Hz
%   sensorLocs          [1x1 or 1xN struct] a single struct for a fixed
%                       instrument or, for a moving one, a vector struct with
%                       one struct per time step; has these fields:
%     sensorLocs.lat    [scalar] latitude estimate of the sensor for each PSD
%     sensorLocs.lon    [scalar] like lats but with longitudes
%     sensorLocs.elev   [scalar] elevation of the sensor above mean sea level
%                       for each PSD; in meters, generally positive for aerial
%                       and terrestrial sensors and negative for subsea ones
%   PSDs                [NxM array] spectra encoded as power spectral density
%                       values; in dB re 1 uPa^2/Hz; each row holds the spectrum
%                       for one time step, each column has the spectrum
%                       values for one frequency, and each third dimension is
%                       one hour of the day; there are as many rows as the
%                       length of timesDt and as many columns as the length of
%                       freqs.

% Generate filename with timestamp in it.
timeStr = datestr(floor(datenum(min(timesDt))), 'yyyymmddTHHMMSS');  % ISO 8601
fname = fullfile(param.psdDir, strrep(param.psdFileTemplate, '%T', timeStr));
if (~isempty(param.psdDir))
  manta.writePsdNetCDF(fname, timesDt, freqs, sensorLocs, PSDs);
end

% Make .png files (for Q/A) of the PSDs and percentiles.
[~,namepart,~] = fileparts(fname);
figure(1)
manta.makePsdImage(param, namepart, timesDt, freqs, PSDs);
figure(2)
manta.makePctileImage(param, namepart, freqs, PSDs);
